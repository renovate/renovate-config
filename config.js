module.exports = {
  $schema: "https://docs.renovatebot.com/renovate-schema.json",
  platform: "gitea",
  endpoint: "https://git.chriswb.dev/api/v1/",
  gitAuthor: "Renovate Bot <renovate.bot@chrisb.xyz>",
  username: "renovate",
  autodiscover: true,
  onboardingConfig: {
    $schema: "https://docs.renovatebot.com/renovate-schema.json",
    extends: ["config:recommended"],
  },
  optimizeForDisabled: true,
  persistRepoData: true,
};
